import MessageHandler from "./MessageHandler";

export default class SocketHandler {

  private _messageHandler: MessageHandler;

  constructor(socket: any) {
    console.log("Socket Handler instantiated");
    this._messageHandler = new MessageHandler(socket);
  }

  public onConnect(): void {
    console.log('A user connected');
    this._messageHandler.sendInitialMessage();
  }

  public onDisconnect(): void {
    console.log('A user disconnected');
  }

  public onMessage(userMessage: string): void {
    this._messageHandler.handleMessage(userMessage);
  }

}
