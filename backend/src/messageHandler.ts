import { EventEmitter } from "events";
import ConciergeHandler from "./ConciergeHandler";
import { IAlicesChoices } from "./IAlicesChoices";

export default class MessageHandler {

    private _socket: EventEmitter;
    private _conciergeHandler: ConciergeHandler | null;

    constructor(socket: any) {
        console.log("Message Handler instantiated");
        this._socket = socket;
        this._conciergeHandler = null;
    }

    public sendInitialMessage(): void {
        this._socket.emit("chat message", "Hey! I'm your bot for today, how can I help?");
    }

    public handleMessage(msg: any): void {
        const usersMessage = msg.toLowerCase();
        const responseMessage = [];

        if (this._conciergeHandler === null) {
            if (this.wasAGreeting(usersMessage)) {
                responseMessage.push("Hi Alice");
            }
            if (this.isARoomRecommendationQuestion(usersMessage)) {
                responseMessage.push(
                    "We have a a range of rooms, all can be seen here:",
                    "https://www.kempinski.com/en/berlin/hotel-adlon/rooms-and-suites/"
                );
            } else if (this.isASurroundingsQuestion(usersMessage)) {
                responseMessage.push(
                    "Take a look at the link below for attractions and things to do around the Adlon:",
                    "https://www.tripadvisor.co.uk/AttractionsNear-g187323-d190694-Hotel_Adlon_Kempinski-Berlin.html"
                );
            } else if (this.isABookingQuestion(usersMessage)) {
                this.beginConciergeProcess(this._socket);
            } else {
                responseMessage.push(
                    "I'm not sure what you're asking...",
                    "I understand questions about the facilities available around the Adlon and ",
                    "I can aid in booking a room."
                );
            }

            responseMessage.forEach(message => this._socket.emit("chat message", message));
        } else {
            if (usersMessage === "exit") {
                this._conciergeHandler = null;
                this._socket.emit("chat message", "Ended the concierge process");
            } else {
                this._conciergeHandler.handleConciergeMessage(usersMessage);
            }
        }

        
    }

    public retrieveAlicesChoices(alicesChoices: IAlicesChoices): void {
        console.log(alicesChoices.room);
        this._conciergeHandler = null;
        this._socket.emit("chat message", "is there anything else I can help you with Ally wally?");
    }

    private wasAGreeting(userMessage: string): boolean {
        const greetingRegExp = /hey|hello|hi|heya|yo|greetings|greeting|helo/;
        const firstWord = userMessage.split(" ")[0].trim();
        return firstWord.match(greetingRegExp) ? true : false;
    }

    private isASurroundingsQuestion(userMessage: string): boolean {
        // perform a check to ensure adlon is 100% there too before returning true
        const surroundingsRegExp = /where|what is there|what's there|adlon|whats there|what can i/;
        return surroundingsRegExp.test(userMessage) ? true : false;
    }

    private isARoomRecommendationQuestion(userMessage: string) {
        const roomRegExp = /wh?at rooms? (would you recommend)?|wha?t rooms?/;
        return roomRegExp.test(userMessage) ? true : false;
    }

    private isABookingQuestion(userMessage: string): boolean {
        const roomRegExp = /i would like to book|to book|could you help me book|book/;
        return roomRegExp.test(userMessage) ? true : false;
    }

    private beginConciergeProcess(socket: EventEmitter) {
        this._socket.emit("chat message", "Right, let's try get you a room");
        this._socket.emit("chat message", "if you wish to stop this process at any time, simple type 'exit'");
        this._socket.emit("chat message", "Type OK to begin :)");
        this._conciergeHandler = new ConciergeHandler(socket, this);
    }

}