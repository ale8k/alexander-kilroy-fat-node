import { EventEmitter } from "events";
import { IAlicesChoices } from "./IAlicesChoices";
import MessageHandler from "./MessageHandler";

export default class ConciergeHandler {

    private _socket: EventEmitter;
    // Had no idea what to do here, thought using a DB for a tech challenge
    // would be a bit over kill, so just gonna store it for Alice in this instance itself.
    private _alicesChoices: IAlicesChoices;
    private _conciergeProgressLevel: number;
    private _conciergeProgressStates: Array<boolean>;
    private _rooms: Array<string>;
    private _messageHandler: MessageHandler;

    constructor(socket: EventEmitter, messageHandler: MessageHandler) {
        this._socket = socket;
        this._alicesChoices = { room: "" };
        this._conciergeProgressLevel = 0;
        // Also didn't make the enum and just put this in instead as well...
        // didn't want to overkill this lol.
        this._conciergeProgressStates = [false, false, false, false, false];
        this._rooms = ["executive", "deluxe", "superior deluxe"];
        this._messageHandler = messageHandler;
    }

    public handleConciergeMessage(msg: any): void {
        const usersMessage = msg.toLowerCase();
    
        switch (this._conciergeProgressLevel) {
            case 0:
                this.decideARoom(usersMessage);
                break;
            case 1:
                this.finishConcierge();
                break;
        }
    }

    private decideARoom(usersMessage: string): void {
        if (this._conciergeProgressStates[0] === false) {
            this._socket.emit("chat message", "Right, come on Alice, what bloody room do you want!? (just type the room name Alice alright luv')");
            this._rooms.forEach(room => this._socket.emit("chat message", room));
            this._conciergeProgressStates[0] = true;
        } else {
            if (this._rooms.indexOf(usersMessage) > -1) {
                this._socket.emit("chat message", `Rightyho Alice, I'll put the ${usersMessage} room down for you...`);
                this._alicesChoices.room = `${usersMessage} room`;
                this._socket.emit("chat message", `Your room is the ${this._alicesChoices.room}, please type something to continue.`);
                this._conciergeProgressLevel = 1;
            } else {
                this._socket.emit("chat message", "That's not a room, try again");
            }
        }
    }

    private finishConcierge(): void {
        this._messageHandler.retrieveAlicesChoices(this._alicesChoices);
    }

}