var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const host = "localhost";

import SocketHandler from "./src/SocketHandler";
import { EventEmitter } from "events";

console.log("Starting app...")


io.on("connection", (socket: EventEmitter) => {

  const socketHandler: SocketHandler = new SocketHandler(socket);

  socketHandler.onConnect();

  socket.on("disconnect", () => {
    socketHandler.onDisconnect();
  });

  socket.on("chat message", (userMessage: any) => {
    socketHandler.onMessage(userMessage);
  });

});

http.listen(3000, host, () => {
  console.log(`listening on ${host}:3000`);
});
