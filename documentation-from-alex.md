# Hi!

To run the backend, please just use "npm run start" or "npm run start-ts" to enable hotreload :).

I added TypeScript and some basic types.

Following our interview, I decided to bring in RxJS to handle the users text, but then removed it
as I felt that was cheating a little :P.

I wasn't sure whether to change the front-end or not? I'm much stronger on front-end so decided
to just do this challenge in node. I did struggle a little with Socket.IO as I honestly just
wanted to get it back super quick, with brexit and all, so apologies if I haven't extensively
used the library as it should've been. I.e., many times I could've used io.once(). Plus this
a side note, this is my first time playing with anything TCP related.

I've also not changed the tests, I'm not very strong on testing, so decided to leave it for this 
challenge and am willing to pick it up properly if granted a position :).

Anyways, it's some basic regex checking in the MessageHandler, and we open an instance
of the ConciergeHandler when the user types a specific kind of input. These's are the
only 3 kind of commands it accepts, I literally copied the user story aha. I hope this is OK!

Oh, lastly, when the concierge finishes I wasn't sure if I should send the link to the room?
Just adding this is as it was a thought I had...

Thanks,
Alex